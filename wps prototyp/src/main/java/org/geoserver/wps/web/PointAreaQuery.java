package org.geoserver.wps.web;

import org.geoserver.feature.ReprojectingFeatureCollection;
import org.geoserver.wps.gs.GeoServerProcess;
import org.geoserver.wps.process.RawData;
import org.geoserver.wps.process.StringRawData;
import org.geotools.data.DataUtilities;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.feature.collection.FilteringSimpleFeatureCollection;
import org.geotools.process.ProcessException;
import org.geotools.process.factory.DescribeParameter;
import org.geotools.process.factory.DescribeProcess;
import org.geotools.process.factory.DescribeResult;
import org.geotools.process.vector.AggregateProcess;
import org.geotools.process.vector.IntersectionFeatureCollection;
import org.locationtech.jts.geom.Geometry;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.filter.Filter;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.*;

@DescribeProcess(title = "PointAreaQuery", description = "Calculate the number of points of the pointLayer by area of the areaLayer. Points per square-km")
public class PointAreaQuery implements GeoServerProcess {

    @DescribeResult(name = "result", description = "Output JSON result")
    public RawData execute(
            @DescribeParameter(name = "areaLayer", description = "Polygon/areal feature collection") SimpleFeatureCollection areaFeatures,
            @DescribeParameter(name = "attributeAreaLayer", description = "Area attribute (e.g. district name), denominator attribute (e.g. total inhabitants)", collectionType = String.class, min = 1) List<String> attributeAreaLayer,
            @DescribeParameter(name = "pointLayer", description = "Point feature collection") SimpleFeatureCollection pointFeatures,
            @DescribeParameter(name = "areaFilter", min = 0, description = "The filter to apply to the area feature layer") Filter areaFilter,
            @DescribeParameter(name = "pointFilter", min = 0, description = "The filter to apply to the point feature layer") Filter pointFilter
    ) throws ClassNotFoundException {

        // apply filtering if necessary
        if (areaFilter != null && !areaFilter.equals(Filter.INCLUDE)) {
            areaFeatures = new FilteringSimpleFeatureCollection(areaFeatures, areaFilter);
        }
        if (pointFilter != null && !pointFilter.equals(Filter.INCLUDE)) {
            pointFeatures = new FilteringSimpleFeatureCollection(pointFeatures, pointFilter);
        }

        /*
            Construct names for grouping and result mapping
         */

        String firstLayerName = areaFeatures.getSchema().getTypeName();
        String groupByName = firstLayerName + "_" + attributeAreaLayer.get(0);

        // Reprojection of geometries if these do not match
        SimpleFeatureIterator areaIterator = areaFeatures.features();
        SimpleFeatureIterator pointIterator = pointFeatures.features();
        if (!areaIterator.next().getDefaultGeometry().getClass().equals(pointIterator.next().getDefaultGeometry().getClass())) {
            try {
                ReprojectingFeatureCollection reproj = new ReprojectingFeatureCollection(pointFeatures, areaFeatures.features().next().getDefaultGeometryProperty().getDescriptor().getCoordinateReferenceSystem());
                pointFeatures = reproj.collection();
            } catch (Exception ex) {
                throw new ProcessException("Different default geometries, cannot perform union");
            }
        }
        areaIterator.close();
        pointIterator.close();


        /*
            Filter out empty point features
         */

        List<SimpleFeature> pointLayerToProcess = new ArrayList<>();
        SimpleFeatureIterator iteratorPoints = pointFeatures.features();
        while (iteratorPoints.hasNext()) {
            SimpleFeature point = iteratorPoints.next();
            Geometry secondGeometry = (Geometry) point.getDefaultGeometry();
            if (secondGeometry == null) {
                // Feature empty - dont add it to the list
                System.out.println("empty feature");
            } else {
                pointLayerToProcess.add(point);
            }
        }
        iteratorPoints.close();

        /*
            Intersect point and polygon layers
            Adding the polygon identifier to the point features
         */

        IntersectionFeatureCollection intersectionFeatureCollection = new IntersectionFeatureCollection();
        SimpleFeatureCollection pointFeaturesWithAreaAttribute = intersectionFeatureCollection.execute(areaFeatures, DataUtilities.collection(pointLayerToProcess), attributeAreaLayer, null, IntersectionFeatureCollection.IntersectionMode.FIRST, false, false);

        /*
            Aggregate the result from above to have point features per polygon identifier
         */

        Set<AggregateProcess.AggregationFunction> FUNCTIONS = new HashSet<AggregateProcess.AggregationFunction>() {
            {
                add(AggregateProcess.AggregationFunction.Count);
            }
        };

        List<String> groupByAttributes = new ArrayList<>();
        groupByAttributes.add(groupByName);
        AggregateProcess.Results groupedResult;
        AggregateProcess agg = new AggregateProcess();
        try {
            groupedResult = agg.execute(pointFeaturesWithAreaAttribute, groupByAttributes.get(0), FUNCTIONS, true, groupByAttributes, null);
        } catch (Exception e) {
            throw new ProcessException("Failed to compute statistics on feature ", e);
        }

        /*
            Create a result list and return as JSON String
         */

        JSONArray results = new JSONArray();
        for (Object[] groupResult : groupedResult.getGroupByResult()) {
            List<Object> resultEntry = new ArrayList<>();
            String areaName = "" + groupResult[0];
            resultEntry.add(areaName);
            double counter = Double.parseDouble("" + groupResult[1]);
            resultEntry.add(counter);
            double areaValue = 0;
            SimpleFeatureIterator iteratorArea = areaFeatures.features();
            while (iteratorArea.hasNext()) {
                SimpleFeature area = iteratorArea.next();
                if (areaName.equals(area.getAttribute(attributeAreaLayer.get(0)))) {
                    areaValue = Math.round((((Geometry) area.getDefaultGeometry()).getArea()/1000000) * 100.0) / 100.0;
                    break;
                }
            }
            iteratorArea.close();
            resultEntry.add(counter / areaValue);
            results.add(resultEntry);
        }

        JSONObject resultsNew = new JSONObject();
        resultsNew.put("GroupByAttributes", attributeAreaLayer);
        resultsNew.put("AggregationResults", results);
        JSONArray aggFunc = new JSONArray();
        aggFunc.add("total-points");
        aggFunc.add("point-by-square-km");
        resultsNew.put("AggregationFunctions", aggFunc);
        resultsNew.put("AggregationAttribute", pointFeatures.getSchema().getTypeName());
        return new StringRawData(resultsNew.toJSONString(), "application/json");
    }
}

// Filtering FeatureCollection by given list of attributes

/* if (firstAttributes != null && firstAttributes.size() > 0) {
            // Always include the geometry
            firstAttributes.add(firstFeatures.features().next().getDefaultGeometryProperty().getName().toString());
            // Now filter for attributes
            String[] names = (String[]) ((String[]) firstAttributes.toArray(new String[firstAttributes.size()]));
            SimpleFeatureType ft = SimpleFeatureTypeBuilder.retype((SimpleFeatureType) ((SimpleFeatureCollection) firstFeatures).getSchema(), names);
            if (!ft.equals(((SimpleFeatureCollection) firstFeatures).getSchema())) {
                firstFeatures = new ReTypingFeatureCollection((SimpleFeatureCollection) firstFeatures, ft);
                System.out.println("we here 1");
            }
        }

        if (secondAttributes != null && secondAttributes.size() > 0) {
            // Always include the geometry
            secondAttributes.add(secondFeatures.features().next().getDefaultGeometryProperty().getName().toString());
            // Now filter for attributes
            String[] names = (String[]) ((String[]) secondAttributes.toArray(new String[secondAttributes.size()]));
            SimpleFeatureType ft = SimpleFeatureTypeBuilder.retype((SimpleFeatureType) ((SimpleFeatureCollection) secondFeatures).getSchema(), names);
            if (!ft.equals(((SimpleFeatureCollection) secondFeatures).getSchema())) {
                secondFeatures = new ReTypingFeatureCollection((SimpleFeatureCollection) secondFeatures, ft);
                System.out.println("we here 2");
            }
        }*/

// My own try of intersect!!!

 /*       SimpleFeatureIterator iteratorArea = firstFeatures.features();
        SimpleFeatureIterator iteratorPoint = secondFeatures.features();
        while (iteratorArea.hasNext()) {
            SimpleFeature firstFeature = iteratorArea.next();
            Geometry firstGeometry = (Geometry) firstFeature.getDefaultGeometry();
            while (iteratorPoint.hasNext()) {
                SimpleFeature secondFeature = iteratorPoint.next();
                Geometry secondGeometry = (Geometry) secondFeature.getDefaultGeometry();
                if (secondGeometry == null) {
                    System.out.println("something emopty");
                    continue;
                }

                if (IntersectUtils.intersects(firstGeometry, secondGeometry)) {
//                    secondFeature.setAttribute(firstAttributes.get(0) + "neu", firstFeature.getAttribute(firstAttributes.get(0)));
                    results.add(secondFeature);
                } else if (firstGeometry.intersects((Geometry) secondFeature.getDefaultGeometry())
                        || firstGeometry.contains((Geometry) secondFeature.getDefaultGeometry())) {
                    results.add(secondFeature);
                }
            }
        }
        */