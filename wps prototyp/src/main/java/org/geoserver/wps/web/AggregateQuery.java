package org.geoserver.wps.web;

import org.geoserver.wps.gs.GeoServerProcess;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.feature.collection.FilteringSimpleFeatureCollection;
import org.geotools.process.factory.DescribeParameter;
import org.geotools.process.factory.DescribeProcess;
import org.geotools.process.factory.DescribeResult;
import org.geotools.process.vector.AggregateProcess;
import org.opengis.filter.Filter;
import org.opengis.util.ProgressListener;

import java.io.IOException;
import java.util.List;
import java.util.Set;

@DescribeProcess(title = "AggregateQuery", description = "Aggregation results (one value for each function computed) - extended by filtering.")
public class AggregateQuery implements GeoServerProcess {

    @DescribeResult(name = "result", description = "Output JSON result")
    public AggregateProcess.Results execute(
            @DescribeParameter(name = "features",description = "Input feature collection") SimpleFeatureCollection features,
            @DescribeParameter(name = "aggregationAttribute",min = 0,description = "Attribute on which to perform aggregation") String aggAttribute,
            @DescribeParameter(name = "function",description = "An aggregate function to compute. Functions include Count, Average, Max, Median, Min, StdDev, Sum and SumArea.",collectionType = AggregateProcess.AggregationFunction.class) Set<AggregateProcess.AggregationFunction> functions,
            @DescribeParameter(name = "featureFilter", min = 0, description = "The filter to apply to the features") Filter filter,
            @DescribeParameter(name = "singlePass",description = "If True computes all aggregation values in a single pass (this will defeat DBMS-specific optimizations)",defaultValue = "false") boolean singlePass,
            @DescribeParameter(name = "groupByAttributes",min = 0,description = "List of group by attributes",collectionType = String.class) List<String> groupByAttributes,
            ProgressListener progressListener
    ) throws ClassNotFoundException, IOException {

        // apply filtering if necessary
        if (filter != null && !filter.equals(Filter.INCLUDE)) {
            features = new FilteringSimpleFeatureCollection(features, filter);
        }

        /*
            Use Geoserver Aggregate Process
         */

        AggregateProcess aggregateProcess = new AggregateProcess();
        return aggregateProcess.execute(features, aggAttribute, functions, singlePass, groupByAttributes, progressListener);
    }
}
