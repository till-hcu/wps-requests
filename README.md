# WPS Requests #

Dieses Repository beinhaltet die im Prototyp verwendeten WPS Prozesse.

## Ordnder wps prototyp ##

Dieser Ordner beinhaltet die Java Klassen der WPS Prozesse:

* PointAreaQuery.java
* PointUnitQuery.java
* AggregateQuery.java

### WPS Requests Erstellen ###

Eine ausführliche Dokumentation hierzu gibt es unter:
https://docs.geoserver.org/latest/en/developer/programming-guide/wps-services/implementing.html

Installation von Maven:
https://docs.geoserver.org/latest/en/developer/maven-guide/index.html#maven-guide

Anschließend werden die #.java Dateien im Pfad '/wps prototyp/src\main\java\org\geoserver\wps\web' abgelegt

Im Vezeichnis '/wps prototyp' wird folgender Maven Befehl ausgeführt
mvn clean install

Die #.jar wird in '/wps prototyp/target' abgelegt und anschließend in den Geoserver Lib Ordner verschoben.

## Ordnder xml tests ##

Dieser Ordner umfasst Beispiele für die mitgelieferten WPS Prozesse des GeoServers als XML und curl Request.

Alle Tests beziehen sich auf die GeoServer Infrastruktur des Prototyps:
https://bitbucket.org/till-hcu/geoserver-docker-new




# WPS Dokumentation #

Teile dieser Dokumentation sind der schriftlichen Ausarbeitung der Machbarkeitsstudie Dashboard-Builder entnommen.
Diese befindet sich in folgendem Repository: https://bitbucket.org/till-hcu/machbarkeitsstudie/src/master/

Der Prototyp, welcher eine Komponente des datenverarbeitenden Backends exemplarisch implementiert, bildet grundlegende Aggregationsfunktionen einer Diagrammerzeugung ab. Hierfür wurden zwei WPS Prozesse implementiert. Diese wurden in JAVA geschrieben und mit Maven zu einem JAR Archiv gebuildet. Die entsprechende Konfiguration der pom.xml, die Archivdatei sowie die JAVA Prozesse sind auf dem entsprechenden Repository vorzufinden. Dieses Archiv wird durch das Hinzufügen zum Lib Ordner des GeoServers installiert. 
Um der prototypischen Entwicklung des vorgestellten Konzepts durch eine gewisse Limitierung einen Rahmen zu geben, sollen an dieser Stelle die zentralen Aggregationsfälle definiert werden. Hierbei werden die bereits vorhandenen WPS Prozesse genutzt sowie zwei neue Prozesse angelegt.

## Aggregation 1 ##

Im WPS Prozess gs:Aggregate kann ein Attribut für eine Aggregationsfunktion ausgewählt werden: 'Count, Average, Max, Median, Min, StdDev, Sum and SumArea'.
```Bsp: Layer - Statgeb_2017_agg, Attribut - unter18J, Funktion - Sum -> Ein Wert mit allen unter18J aller StatGeb```
Hierdurch wird ein einzelner Wert zurückgegeben, welche für eine KPI verwendet werden könnte. Für eine diagrammatische Darstellug muss eine Spalte zur Gruppierung der Daten definiert werden.
```Bsp: Layer - Statgeb_2017_agg, Attribut - unter18J, Funktion - Sum, groupByAttributes - Stadtteil-> Ein Wert mit allen unter18J pro StatGeb```
Der entsprechende XML Request für diese und alle anderen beschriebenen Fäll sind im entsprechenden Repository hinterlegt.

## Aggregation 2 ##

Verschneidung mehrerer Layer: Wie viele X (einer Point-Feature Layer) existieren pro Flächeneinheit (Bezirk, Stadtteil, St_Geb etc.) pro Bevölkerungseinheit (Gesamt, Männer, Frauen etc.). Hierfür wird dem WPS Prozess der Layername und die Attribute für die Berechnung des Nenners übergeben sowie der Layername des Zählers. Im exemplarischen XML Request (PointUnitQuery.xml) ist der Nenner Layer 'builder:Stadtteile_2017_agg', die Flächeneinheit ist 'stadtteil' und die Bevölkerungseinheit 'Insgesamt'. Für den Zähler wird lediglich der Name der Punktelayer übergeben 'builder:KitaEinrichtungen'.

## Aggregation 3 ##

Verschneidung mehrerer Layer: Wieviele X (einer Point-Feature Layer) existieren pro Flächeneinheit (Bezirk, Stadtteil, St_Geb etc.) pro km². Hierfür wird dem WPS Prozess der Layername und der Name der Flächeneinheit für die Berechnung des Nenners übergeben sowie der Layername des Zählers. Im exemplarischen XML Request (PointAreaQuery.xml) ist der Nenner Layer 'builder:Stadtteile_2017_agg' und die Flächeneinheit ist 'stadtteil'. Für den Zähler wird lediglich der Name der Punktelayer übergeben z.B. 'builder:KitaEinrichtungen'.

## Filter ##

Um die genannten Aggregationen mit der Möglichkeit der Filterung auszustatten, werden CQL Filter verwendet. Hierdurch können beispielsweise die Aggregationen 2 und 3 mit einem Filter für Stadtteile oder Bezirke versehen werden. Die CQL Filter sollen später im Frontend abgebildet werden können und die Filterung eines Diagrammes oder mehrerer Komponenten eines Dashboards ermöglichen. Für die Verwendung von CQL werden dem Query ein einzelner oder mehrere wps:input hinzugefügt:

```
<wps:Input key=”pointFilter” inputType=”filter”>
   <ows:Identifier>pointFilter</ows:Identifier>
   <wps:Data>
      <wps:ComplexData mimeType="text/plain; subtype=cql"><![CDATA[Traeger LIKE 'Elbkinder%']]></wps:ComplexData>
   </wps:Data>
 </wps:Input>
```
Mehrere: 
```
<wps:Input key=”pointFilter” inputType=”filter”>
    <ows:Identifier>areaFilter</ows:Identifier>
    <wps:Data>
      <wps:ComplexData mimeType="text/plain; subtype=cql"><![CDATA[bezirk IN('Altona', ‘Harburg’)]]></wps:ComplexData>    </wps:Data>
</wps:Input>
```

Diese optionalen Filter werden für alle angefragten Layer als optionale Prozessparameter hinzugefügt.
WPS Parameter 
Zur Veranschaulichung der Funktionsweise soll an dieser Stelle grob der PointUnitQuery (zuvor erwähnte Aggregation 2) erläutert werden. Dieser Query nimmt fünf Parameter entgegen:

* name = "areaLayer", description = "Polygon/areal feature collection"
* name = "attributeAreaLayer", description = "Area attribute (e.g. district name), denominator attribute (e.g. total inhabitants)"
* name = "pointLayer", description = "Point feature collection"
* name = "areaFilter", description = "The filter to apply to the area feature layer"
* name = "pointFilter", description = "The filter to apply to the point feature layer"

Die ausgeführte Kalkulation liefert ein JSON zurück mit der Anzahl der Punkt der 'PointLayer' pro Einheit des definierten Attributes der 'AreaLayer'. Die Ergebnisse umfassen hierbei die Punkte pro 100.000, 10.000 und 1000 Einheiten der 'AreaLayer'. Beide Layer können durch das Hinzufügen von CQL Filtern eingeschränkt werden. Da die Layer des Masterportals über ein einheitliches Koordinatensystem verfügen, wurde auf die Implementierung der Umrechnung der Koordinatensystems bei Differenz zwischen den Layern verzichtet. Dies ist aber technisch unproblematisch und gut dokumentiert und bereits in den Prozessen vorbereitet und derzeit auskommentiert.

## Datenquellen ##

Für Regelfall geht der Prototyp davon aus, dass die zu aggregierenden Daten aus dem Geoserver (analog zum deegree der UDP) über einen im WPS eingebetteten WFS GetFeature Request (in Zukunft vsl. zu erweitern auf OGC API Features) geladen werden. Alternativ ist es jedoch möglich, auch Rohdaten direkt als GeoJSON oder GML im RequestBody mit zu übergeben.
Im Rahmen der Studie wird davon ausgegangen, dass somit auch externe Daten (z.B. aus Sensor-APIs) direkt und über den gleichen Ansatz eingespielt werden können und i.d.R. keine Doppelung/Spiegelung der Aggregations-Funktionen auf mehreren Systemen notwendig ist. Dieser Ansatz wird für in Kapitel 5.1.3 noch einmal detailliert beschrieben.

## Berechnungsergebnisse ##

Die Ergebnisse aller WPS-Prozesse sollten nach dem gleichen Schema, orientiert am WPS-Standard formatiert werden. Im hier gelieferten Beispiel könnte z.B. ein gruppiertes Balkendiagramm erzeugt werden, bei dem die Werte für „total-points“ und „point-by-squre-km“ der Straßenbäume in einer Gruppe pro Stadtteil dargestellt werden.
```
{
// Die Bezeichner nach denen die Ergebnisse im Chart gruppiert und bezeichnet werden.
// z.B. die Bennennung der X-Achse bei einem Balkendiagramm
// Bei mehreren Attributen wären die Attribute so geschachtelt, dass der erste Wert dem zweiten übergeordnet wäre
        "GroupByAttributes": [
            "stadtteil"
        ],
// die ausgeführten Funktionen
// bzw. die Bezeichner der einzelnen Werte innerhalb einer Gruppe
// z.B. die Bennennung der Balken in einem gruppierten Balkendiagramm
        "AggregationFunctions": [
            "total-points",
            "point-by-square-km"
        ],
// Der Bezeichner des verarbeiteten Werts
// eine Gruppe im Balkendiagramm könnte z.B. „Strassenbaeume: Total-Points“ benannt sein.
        "AggregationAttribute": "strassenbaeume",
// Die Ergebnisse pro Gruppe
// Der Index des Eintrags korrespondiert aufsteigend mit den Bezeichnern in „GroupByAttributes“ und „AggregationFunctions“
        "AggregationResults": [
            [
                "Rönneburg", // GroupByAttributes[0]
                187.0, // AggregationFunctions[0]
                83.11 // AggregationFunctions[1]
            ]
        ],
    }
```

## Architecture

Link to Schema: https://miro.com/app/board/o9J_khFZLbE=/

![Architecture_Schema](https://bitbucket.org/till-hcu/builder-back-end/downloads/architektur.png)